DROP DATABASE IF EXISTS promo14_kamel_blog;
CREATE DATABASE promo14_kamel_blog;
USE promo14_kamel_blog;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`(  
    `id` int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `nom` varchar(60)NOT NULL,
    `prenom` varchar(60) NOT NULL,
    `email` VARCHAR (100) NOT NULL UNIQUE,
    `password` VARCHAR (255) NOT NULL,
    `role` VARCHAR (40) NOT NULL
) DEFAULT CHARSET=utf8mb4 comment '';



DROP TABLE IF EXISTS post;
CREATE TABLE `post` (
    `id`  int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `date`DATE NOT NULL,
    `auteur` VARCHAR (252) NOT NULL,
    `titre` VARCHAR (252) NOT NULL,
    `texte` TEXT NOT NULL,
    `picture` VARCHAR (252),
    user_id int,
     KEY `FK_UserPost` (`user_id`),
    CONSTRAINT `FK_UserPost` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE SET NULL  ON UPDATE SET NULL
) DEFAULT CHARSET=utf8mb4 comment '';


