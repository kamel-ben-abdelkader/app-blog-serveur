import { Router } from "express";
import { PostRepository } from "../repository/PostRepository";
import { Post } from "../entity/Post";
import { protect } from "../utils/token";
import { uploader } from "../uploader";


export const postController = Router();

postController.get('/', async (req, res) => {
    try {
        let posts;
        if (req.query.search) {
            posts = await PostRepository.search(req.query.search);
        } else {
            posts = await PostRepository.getAllPosts();
        }
        res.json(posts)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.get('/:id', async (req, res) => {
    try {
        res.json(await PostRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


postController.get('/user/:userId', async (req, res) => {
    try {
        res.json(await PostRepository.findByUserId(req.params.userId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})



//======================================Post Protected Route ================================//

postController.post('/', uploader.single('picture'), protect(), async (req, res) => {
    try {
        const newPost = new Post();
         // Assigning req.body to New Post
        Object.assign(newPost, req.body);

        newPost.picture = '/uploads/' + req.file.filename;
    


        await PostRepository.add(newPost, req.user);
        res.status(201).json(newPost);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//======================================Patch Protected Route ================================//

postController.patch('/:id', protect(), async (req, res) => {
    try {
        let data = await PostRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        if (data.userId !== req.user.id) {
            res.send(401).end();
            return
        }
        let update = { ...data, ...req.body };

        await PostRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//====================================== Delete Protected Route ================================//

postController.delete('/:id', protect(), async (req, res) => {
    try {
        let toDelete = await PostRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'admin') {
            await PostRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})


//====================================== In progress, addition of likes, comments for an improved version ================================//



// postController.get('/:id/comment', async(req,res)=>{
//     try {
//         res.json(await CommentRepository.getCommentsForPost(req.params.id))
//     } catch (error) {
//         console.log(error);
//         res.status(400).end();
//     }
// })

// postController.post('/:id/like', passport.authenticate('jwt', { session: false }), async(req,res)=>{
//     try {
//         await PostRepository.like(req.user.id,req.params.id);
//         res.status(201).end()
//     } catch (error) {
//         console.log(error);
//         res.status(400).end();
//     }
// })

// postController.delete('/:id/like', passport.authenticate('jwt', { session: false }), async(req,res)=>{
//     try {
//         await PostRepository.dislike(req.user.id,req.params.id);
//         res.status(204).end()
//     } catch (error) {
//         console.log(error);
//         res.status(400).end();
//     }
// })
