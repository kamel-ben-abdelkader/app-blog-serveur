import { User } from "../entity/User";
import { connection } from "./connection";


export class UserRepository {
    /**
     * Method that persists a user
     * @param {User} user user to persist
     */
    static async add(user) {
        const [rows] = await connection.query('INSERT INTO user (nom,prenom,email,password,role) VALUES (?,?,?,?,?)', [user.nom, user.prenom, user.email, user.password, user.role]);
        user.id = rows.insertId;
    }
    /**
     * Method that find user with a specific email in db
     * @param {string} email 
     * @returns {Promise<User>} Found user or null
     */
    static async findByEmail(email) {
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email]);
        if (rows.length === 1) {
            return new User(rows[0].nom, rows[0].prenom, rows[0].email, rows[0].password, rows[0].role, rows[0].id);
        }
        return null;

    }

    static async delete(id) {
        await connection.execute(`DELETE FROM user WHERE id=?`, [id])
    };

    static async update(user) {
        await connection.execute('UPDATE user SET nom=?, prenom=?, email=?, password=?, role=? WHERE id=?', [user.nom, user.prenom, user.email, user.password, user.role, user.id]);
    };



    static async findById(id) {
        let [rows] = await connection.execute(`SELECT * FROM user WHERE id=?`, [id]);
        return new User(rows[0].nom, rows[0].prenom, rows[0].email, rows[0].password, rows[0].role, rows[0].id);
    }

}