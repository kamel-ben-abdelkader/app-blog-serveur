import { Post } from "../entity/Post";
import { User } from "../entity/User";
import { connection } from "./connection";

export class PostRepository {




    static async getAllPosts() {
        const [rows] = await connection.execute('SELECT * FROM post ORDER BY date DESC');
        return rows.map(item => new Post(item['date'], item['auteur'], item['titre'], item['texte'], item['picture'], item['user_id'], item['id']));
    };


      //======================================== Method FindAll with join ====================================//

    // /**
    //  * Find every rows in operations table
    //  * @returns 
    //  */
    //  static async findAllPost() {


    //     const [rows] = await connection.execute({ sql: 'SELECT * FROM post LEFT JOIN user ON post.user_id = user.id', nestTables: true });
    //     console.log(rows);
    //     const posts = [];
    //     for (const row of rows) {
    //         let post = new Post(row.post.date, row.post.auteur, row.post.titre, row.post.texte, row.post.picture, row.post.userId, row.post.id);
    //         post.user = new User(row.user.nom, rows.user.prenom, rows.user.email, rows.user.password, rows.user.role, row.user.id)


    //         posts.push(post);

    //     }
    //        console.log(posts);
    //     return posts;


    // }

    static async add(post, user) {
        const [row] = await connection.execute('INSERT INTO post (date,auteur,titre,texte,picture,user_id) VALUES (?,?,?,?,?,?)', [post.date, post.auteur, post.titre, post.texte, post.picture, user.id]);
        return post.id = row.insertId;
    };

    static async delete(id) {
        await connection.execute(`DELETE FROM post WHERE id=?`, [id]);
    };

    static async update(post) {
        await connection.query('UPDATE post SET titre=?, texte=?, picture=? WHERE id=?', [post.titre, post.texte, post.picture, post.id]);
    };


    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM post WHERE id=?`, [id]);
        return new Post(row[0]['date'], row[0]['auteur'], row[0]['titre'], row[0]['texte'], row[0]['picture'], row[0]['user_id'], row[0]['id']);
    }

    static async findByUserId(userId) {
        let [rows] = await connection.execute(`SELECT * FROM post WHERE user_id=?`, [userId]);
        return rows.map(item => new Post(item['date'], item['auteur'], item['titre'], item['texte'], item['picture'], item['user_id'], item['id']));
    }

    static async search(search) {
        const [rows] = await connection.execute(`SELECT * FROM post WHERE CONCAT(auteur,titre) LIKE ? ORDER BY date DESC`, ['%' + search + '%']);
        return rows.map(item => new Post(item['date'], item['auteur'], item['titre'], item['texte'], item['picture'], item['user_id'], item['id']));
    };


}

