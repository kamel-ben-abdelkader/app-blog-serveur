


export class User {
    id;
    nom;
    prenom;
    email;
    password;
    role;

    /**
     * 
     * @param {string} nom 
     * @param {string} prenom 
     * @param {string} email 
     * @param {string} password 
     * @param {string} role 
     * @param {number} id 
     */
    constructor(nom, prenom, email, password, role='user', id = null) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
        this.role = role;
        this.id = id;

    }
//Objet to Json, Voluntary oversight of the password to ensure that it circulates as little as possible
    toJSON() {
        return {
            id: this.id,
            email: this.email,
            role: this.role
        }
    }
}