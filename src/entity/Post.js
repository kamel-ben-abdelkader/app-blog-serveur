
export class Post {
    id;
    date;
    auteur;
    titre;
    texte;
    picture;
    userId;
    user;
/**
 * 
 * @param {Date} date 
 * @param {string} auteur 
 * @param {string} titre 
 * @param {string} texte 
 * @param {string} picture 
 * @param {number} userId 
 * @param {number} id 
 */
    constructor(date, auteur, titre, texte, picture = null, userId, id) {
        this.date = date;
        this.auteur = auteur;
        this.titre = titre;
        this.texte = texte;
        this.picture = picture;
        this.userId = userId;
        this.id = id;
    }
}
