import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { postController } from './controller/post-controller';

import { configurePassport } from './utils/token';
import passport from 'passport';

//Call of the function that configures the JWT strategy
configurePassport();

export const server = express();

//passport use to authorize or not access to roads
server.use(passport.initialize());

server.use(express.json());
server.use(cors({}));
server.use(express.static('public'));


server.use('/api/user', userController);

server.use('/api/post', postController);