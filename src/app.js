import 'dotenv-flow/config';
import { server } from './server';
// import { generateToken } from './utils/token';

const port = process.env.PORT || 8000;


server.listen(port, ()=> {
    console.log('listening on port '+8000);
});



// console.log(generateToken({
//     email: 'test@gmail.com',
//     id: 1,
//     role: 'user'
// }, 99999));
